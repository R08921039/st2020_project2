const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 



function success(position) {
    var latitude  = position.coords.latitude;
    var longitude = position.coords.longitude;
    page.setGeolocation({
        latitude: latitude,
        longitude: longitude
    });
    

    
};

  function error() {
    output.innerHTML = "Unable to retrieve your location";
}




// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button).toBe('Join');
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R4', 100);
    await page.keyboard.press('Enter', {delay: 100});
    await page.screenshot({path: 'test/screenshots/welcome.png'});
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.keyboard.press('Enter', {delay: 100});
    await page.screenshot({path: 'test/screenshots/errmsg.png'});
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R6', 100);
    await page.keyboard.press('Enter', {delay: 1});

    await page.waitForSelector('#swal2-title');
    let msg = await page.evaluate(() => {
        return document.querySelector('#swal2-title').innerHTML;
    });
    expect(msg).toBe('Welcome!');

    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R8', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('R8');

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R9', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {

    const browser = await puppeteer.launch({ headless: !openBrowser });
    
    const page1 = await browser.newPage();
    const page2 = await browser.newPage();
    
    await page1.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R10', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100});

    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R10', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page1.bringToFront();
    await page1.waitForSelector('#users > ol > li:nth-child(1)');
      
    let member1_1 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    await page1.waitForSelector('#users > ol > li:nth-child(2)');

    let member1_2 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    
    expect(member1_1).toBe('John');
    expect(member1_2).toBe('Mike');

    await page2.bringToFront();
    await page2.waitForSelector('#users > ol > li:nth-child(1)');
      
    let member2_1 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    await page2.waitForSelector('#users > ol > li:nth-child(2)');

    let member2_2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    
    expect(member2_1).toBe('John');
    expect(member2_2).toBe('Mike');
    
    await browser.close();
    
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R11', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    
    await page.waitForSelector('#message-form > button');
    let button = await page.evaluate(() => {
        return document.querySelector('#message-form > button').innerHTML;
    });
    expect(button).toBe('Send');
    await browser.close();
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R12', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#message-form > input[type=text]');
    await page.type('#message-form > input[type=text]', 'hi', {delay: 100});
    
     
    await page.waitFor(2000);
    await (await page.$('#message-form > button')).click();
    


    let msg_from = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
    });
    expect(msg_from).toBe('John');
    let msg = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });

    expect(msg).toBe('hi');
    await browser.close();
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    
    const page1 = await browser.newPage();
    const page2 = await browser.newPage();

    await page1.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R13', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100});
    await page1.waitForSelector('#users > ol > li'); 

    
    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R13', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100});
    await page2.waitForSelector('#users > ol > li:nth-child(2)');


    await page1.bringToFront();
    await page1.waitForSelector('#message-form > input[type=text]');
    await page1.type('#message-form > input[type=text]', 'Hi', 100);
    await page1.waitFor(2000);
    await (await page1.$('#message-form > button')).click();
    



    await page2.bringToFront();
    await page2.waitForSelector('#message-form > input[type=text]');
    await page2.type('#message-form > input[type=text]', 'Hello', 100);
    await page2.waitFor(2000);
    await (await page2.$('#message-form > button')).click();
    

    
    let msg_from1 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
    });
    expect(msg_from1).toBe('John');

    
    let msg1 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });

    expect(msg1).toBe('Hi');


    

    await page1.bringToFront();
   
    let msg_from2 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(1) > h4').innerHTML;
    });
    expect(msg_from2).toBe('Mike');

    
    let msg2 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(2) > p').innerHTML;
    });

    expect(msg2).toBe('Hello');

    await browser.close();
    
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R14', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#send-location');
    let button = await page.evaluate(() => {
        return document.querySelector('#send-location').innerHTML;
    });
    expect(button).toBe('Send location');
    await browser.close();
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    
    const context = browser.defaultBrowserContext();
    await context.overridePermissions(url, ['geolocation']);

    const page = await browser.newPage();
    await page.goto(url);
    
    
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R15', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    
    
    
     
    await page.waitFor(2000); 
    await (await page.$('#send-location')).click();

    

    await page.waitForSelector('#send-location');    
    let location_button = await page.evaluate(() => {
        return document.querySelector('#send-location').innerHTML;
    });
    expect(location_button).toBe('Sending location...');



})
